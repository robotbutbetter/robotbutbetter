package com.worawit.robotproject;

public class Fuel extends Obj{
	int volumn;
	public Fuel (int x, int y,int volumn){
            super('F',x,y);
            this.volumn = volumn;
	}
	public int fillFuel (){
		int vol = volumn;
		symbol = '-';
		volumn = 0;
		return vol;
	}
	public int getVolumn (){
		return volumn;	
	}

}